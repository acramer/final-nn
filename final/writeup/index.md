# Final project writeup

## Intro
This project was developed in 4 different parts, data collection, controller development, puck classification, and puch detection.

### Hardware
We bought a RTX 2080S from best buy to train our models at home and on the same machine that generated the data. However, one our desktop's motherboard was too old to handle the card. We decided to collect data on laptops and then train our models on GCP machines with K80s.

### Initial thoughts
We wanted to do some sort of blend between imitation learning and then continueing to learn policy and Q-function with DDPG... lol.

### Reality
Decided to use a classifier and detector. Wrote the controller through playing and expereimentation.

## Data Collection
We recognized early that we would need a great deal of data for the two datasets so  we decided that we would automate the process. 
We also felt that collecting data either manually or through a controller with special knowdege could lead to a
under-representation of conditions our agent might have found itself in.  So our first solution was to use the AI provided by the game.
The advantage in doing so was that certain attributes such as 'kart', 'team', and 'difficulty' could be randomly selected to reduce the risk of bias.

We automated the data collection by modifying and brining over the "rollout" function from hw6.
With this setup we were able to accesss the pucks position and its image coordinates with it.
We then used this, in combinations with a few other bits of information  to determine if a picture would have 
the puck in it.

Unltimately We were disatisfied with this approach so we decided to further automate with the new control over the puck and player position.
We created an environment that teleported the player to a random point near the middle of the map every 50 steps.  During that time,
the puck was spawned into 50 differeent positions in front of the kart and a image was taken at each step.



## Controller Development
Trained this by using the ground truth.
* Started by wiggling toward the puck. We get the angle using arccos.
* Continue with wiggle algorithm but use the carts angle to the goal to adjust. Also added drifting larger angles that are still on the screen.
* Replaced drifting when the ball went off screen with turning backward in the opposite direction that the ball flew off the screen.
* Scrapped using cart angle to the goal, and instead used the puck angle to the goal to adjust the aim angle.
** Added a factor of 1.1 to this adjustment.
* Changed to `gavroche` for more speed
* Found to be winning by two or three points every time on `difficulty=2`

## Puck Classification
Initially we trained a CNN from Homeework 4 solution.  However I was unaable to get it and the FCN to peformm

Added a different solution form homework 3. A little simpler architecture. Found that this never get a respectably low enough loss - plateaued around 0.01. This led us to add downsampling to the archicture using `AvgPool(5, stride=4)`. This improved training speed by a ridiculous amount and improved performance quite a bit. Then we swithc ed to a new dataset that made it harder to get such a low loss. However, continued using the downsampling to keep up the fast training times.

## Puck Detection

For this we use the network from homework 6. 
Given a image as input, we produce a logit of the same size spacially and regress to the x and y coordinates determined from the datast collection step.

We attemped to train the fcn with some negative examples to penilize sparsely activated networks.
However we were unable to get around the problem with batching across two types of input with two types of loss.
