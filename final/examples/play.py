import matplotlib.pyplot as plt
import matplotlib.lines as lines
from matplotlib.lines import Line2D

from pathlib import Path
from PIL import Image
import argparse
import pystk
from time import time
import numpy as np
from . import gui

import random
from scipy.spatial.transform import Rotation as R
from math import cos, sin, pi

import importlib
from os import path

def action_dict(action):
    return {k: getattr(action, k) for k in ['acceleration', 'brake', 'steer', 'fire', 'drift']}

def to_numpy(location):
    """
    Don't care about location[1], which is the height
    """
    return np.float32([location[0], location[2]])


def get_vector_from_this_to_that(me, obj, normalize=True):
    """
    Expects numpy arrays as input
    """
    vector = obj - me

    if normalize:
        return vector / np.linalg.norm(vector)

    return vector

def to_image(config, x, camera):
    proj, view = np.array(camera.projection).T, np.array(camera.view).T
    W, H = config.screen_width, config.screen_height
    p = proj @ view @ np.array(list(x) + [1])
    return np.array([W / 2 * (p[0] / p[-1] + 1), H / 2 * (1 - p[1] / p[-1])])

def on_screen(config, x, camera, angle=0):
    if abs(angle) > 100: return False
    coords = to_image(config, x, camera)
    return coords[0] > 0 and coords[0] < config.screen_width and coords[1] > 0 and coords[1] < config.screen_height

def to_world(config, aim_point_image, camera, height=0):
    proj, view = np.array(camera.projection).T, np.array(camera.view).T
    x, y, W, H = *aim_point_image, config.screen_width, config.screen_height
    pv_inv = np.linalg.pinv(proj @ view)
    xy, d = pv_inv.dot([float(x) / (W / 2) - 1, 1 - float(y) / (H / 2), 0, 1]), pv_inv[:, 2]
    x0, x1 = xy[:-1] / xy[-1], (xy+d)[:-1] / (xy+d)[-1]
    t = (height-x0[1]) / (x1[1] - x0[1])
    if t < 1e-3 or t > 10:
        # Project the point forward by a certain distance, if it would end up behind
        t = 10
    return t * x1 + (1-t) * x0

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-k', '--kart', default='')
    parser.add_argument('--team', type=int, default=0, choices=[0, 1])
    parser.add_argument('-s', '--step_size', type=float)
    parser.add_argument('-n', '--num_player', type=int, default=1)
    parser.add_argument('-v', '--visualization', type=str, choices=list(gui.VT.__members__), nargs='+',
                        default=['IMAGE'])
    parser.add_argument('-V', '--verbose', action='store_true')
    parser.add_argument('-N', '--num_images', type=int, default=50)
    parser.add_argument('--save_dir', type=Path, required=False)
    args = parser.parse_args()

    if args.save_dir:
        args.save_dir.mkdir(parents=True, exist_ok=True)

    g_config = pystk.GraphicsConfig.hd()
    g_config.screen_width = 400
    g_config.screen_height = 300
    pystk.init(g_config)

    config = pystk.RaceConfig()
    config.num_kart = 4
    config.num_kart = 1
    if args.kart is not None and args.kart != '':
        config.players[0].kart = args.kart
    else:
        config.players[0].kart = 'gavroche'

    config.players[0].controller = pystk.PlayerConfig.Controller.PLAYER_CONTROL
    config.players[0].team = args.team

    # AI 1
    config.players.append(pystk.PlayerConfig('gavroche', pystk.PlayerConfig.Controller.AI_CONTROL, args.team))
    enemy_kart = random.choice(['adiumy', 'amanda', 'beastie', 'emule', 'gavroche', 'gnu', 'hexley', 'kiki', 'konqi', 'nolok', 'pidgin', 'puffy', 'sara_the_racer', 'sara_the_wizard', 'suzanne', 'tux', 'wilber', 'xue'])
    # Enemy AI
    config.players.append(pystk.PlayerConfig(enemy_kart, pystk.PlayerConfig.Controller.AI_CONTROL, (args.team + 1) % 2))
    config.players.append(pystk.PlayerConfig(enemy_kart, pystk.PlayerConfig.Controller.AI_CONTROL, (args.team + 1) % 2))


    config.track = "icy_soccer_field"
    config.mode = config.RaceMode.SOCCER

    if args.step_size is not None:
        config.step_size = args.step_size

    race = pystk.Race(config)
    race.start()

    uis = [gui.UI([gui.VT[x] for x in args.visualization]) for i in range(args.num_player)]

    state = pystk.WorldState()
    race.step()

    t0 = time()
    n = 0

    # ax = plt.gcf().add_subplot(3, 3, 9)

    count = 2500

    # ball_on_field = False
    ball_on_field = True
    # state.set_ball_location([0,-10,0])

    while all(ui.visible for ui in uis):
        # Set other Kart locations to under map
        if not all(ui.pause for ui in uis):
            race.step(uis[0].current_action)
            state.update()
            if args.verbose and config.mode == config.RaceMode.SOCCER:
                print('Score ', state.soccer.score)
                print('      ', state.soccer.ball)
                print('      ', state.soccer.goal_line) 

        # # Vector and Orientation Calculations
        # if count % 50 == 0:
        #     # Part 1 - Randomly place kart in Boundary A
        #     psi = random.random()*360
        #     rko = R.from_euler('y',psi,degrees=True).as_quat()
        #     rkx, rky = (random.random()-0.5)*50,(random.random()-0.5)*80
        #     state.set_kart_location(state.players[0].kart.id, [rkx, 1, rky], rko)
        # # Select Random distance and random angle
        # dist = random.random()*(abs(random.choice([rkx-35,rkx+35,rky-50,rky+50]))-1.5)+1.5
        # angle = (random.random()*90-45+psi) * pi/180 - pi/2
        # # Calculate Vector to puck
        # pos_ball = (rkx + cos(angle)*dist, rky - sin(angle)*dist)
        # # Ensure it is within boundaries
        # pos_ball = (max(min(pos_ball[0], 35), -35), max(min(pos_ball[1], 50), -50))
        # # Change Puck Position
        # state.set_ball_location([pos_ball[0],0.5,pos_ball[1]])

        # # Corner Display
        # # Vector and Orientation Calculations
        # if count % 50 == 0:
        #     # Part 1 - Randomly place kart in Boundary A
        #     psi = random.random()*360
        #     rand_kart_orientation = R.from_euler('y',psi,degrees=True).as_quat()
        #     rand_kart_x, rand_kart_y = (random.random()-0.5)*50,(random.random()-0.5)*80
        #     state.set_kart_location(state.players[0].kart.id, [rand_kart_x, 1, rand_kart_y], rand_kart_orientation)

        #     # Select Random distance and random angle
        #     rand_puck_dist = random.random()*(max([abs(rand_kart_x - 35), abs(rand_kart_x + 35), abs(rand_kart_y - 50), abs(rand_kart_y + 50)]) - 1.5) + 1.5
        #     rand_puck_angle = (random.random()*90 - 45 + psi) * pi/180 - pi/2

        #     # Calculate Vector to puck
        #     pos_ball =   (cos(rand_puck_angle)*rand_puck_dist + rand_kart_x,
        #                -1*sin(rand_puck_angle)*rand_puck_dist + rand_kart_y)

        #     pos_ball = (max(min(pos_ball[0], 35), -35), max(min(pos_ball[1], 50), -50))

        #     state.set_ball_location([pos_ball[0],0.5,pos_ball[1]])
        #     state.update()
        # #elif count > 50 and count % 1 == 0:
        # else:
        #     rand_puck_dist = random.random()*(max([abs(rand_kart_x - 35), abs(rand_kart_x + 35), abs(rand_kart_y - 50), abs(rand_kart_y + 50)]) - 1.5) + 1.5
        #     rand_puck_angle = (random.random()*90 - 45 + psi) * pi/180 - pi/2
        #     # Calculate Vector to puck
        #     pos_ball =   (cos(rand_puck_angle)*rand_puck_dist + rand_kart_x,
        #                -1*sin(rand_puck_angle)*rand_puck_dist + rand_kart_y)
        #     pos_ball = (max(min(pos_ball[0], 35), -35), max(min(pos_ball[1], 50), -50))
        #     state.set_ball_location([pos_ball[0],0.5,pos_ball[1]])

        pos_ball = to_numpy(state.soccer.ball.location)
        pos_me = to_numpy(state.players[0].kart.location)
        front_me = to_numpy(state.players[0].kart.front)
          
        ori_me = get_vector_from_this_to_that(pos_me, front_me)
        ori_to_ball = get_vector_from_this_to_that(pos_me, pos_ball)
          
        # # Plotting
        # aim_point_image = to_image(g_config, state.soccer.ball.location, state.players[0].camera)
        # world_point = to_world(g_config, aim_point_image, state.players[0].camera, state.soccer.ball.location[1])
        # ax.clear()
        # ax.set_xlim(-100, 100)
        # ax.set_ylim(-100, 100)
          
        # ax.plot(pos_me[0], pos_me[1], 'r.')                 # Current player is a red dot.
        # ax.plot(pos_ball[0], pos_ball[1], 'co')             # The puck is a cyan circle.
        # # Draw B Boundary
        # B1, B2, B3, B4 = (-35, 50), (35, 50), (35, -50), (-35, -50)
        # x1, y1, x2, y2, x3, y3, x4, y4 = B1[0], B1[1], B2[0], B2[1], B3[0], B3[1], B4[0], B4[1]
        # ax.add_line(Line2D((x1, x2), (y1, y2), linewidth=2, color='blue')) 
        # ax.add_line(Line2D((x3, x2), (y3, y2), linewidth=2, color='blue')) 
        # ax.add_line(Line2D((x3, x4), (y3, y4), linewidth=2, color='blue')) 
        # ax.add_line(Line2D((x1, x4), (y1, y4), linewidth=2, color='blue')) 
          
        # # Draw A Boundary
        # A1, A2, A3, A4 = (-25, 40), (25, 40), (25, -40), (-25, -40)
        # x1, y1, x2, y2, x3, y3, x4, y4 = A1[0], A1[1], A2[0], A2[1], A3[0], A3[1], A4[0], A4[1]
        # ax.add_line(Line2D((x1, x2), (y1, y2), linewidth=2, color='green')) 
        # ax.add_line(Line2D((x3, x2), (y3, y2), linewidth=2, color='green')) 
        # ax.add_line(Line2D((x3, x4), (y3, y4), linewidth=2, color='green')) 
        # ax.add_line(Line2D((x1, x4), (y1, y4), linewidth=2, color='green')) 
          
          
        # # Plot lines of where I am facing, and where the enemy is in relationship to me.
        # ax.plot([pos_me[0], pos_me[0] + 10 * ori_me[0]], [pos_me[1], pos_me[1] + 10 * ori_me[1]], 'r-')
        # ax.plot([pos_me[0], pos_me[0] + 10 * ori_to_ball[0]], [pos_me[1], pos_me[1] + 10 * ori_to_ball[1]], 'c-')
          
        angle = (1 if (ori_me[1]*ori_to_ball[0]-ori_me[0]*ori_to_ball[1]) > 0 else -1)*np.degrees(np.arccos(np.dot(ori_me, ori_to_ball)))
        is_on_screen = on_screen(g_config, state.soccer.ball.location, state.players[0].camera, angle)
        # dist_to_puck = np.linalg.norm(pos_me-pos_ball)
          
        take_pic = is_on_screen and abs(pos_me[1]) < 63 and abs(float(state.soccer.ball.location[1])) < 2
        # # Displaying
        # ax.set_title('{:=5.1f} , {:=5.1f} | {:=5.1f} '.format(*pos_me, dist_to_puck))


        for ui, d in zip(uis, race.render_data):
            ui.show(d)

        if n % 2 == 0 and args.save_dir and count < args.num_images:
            if not ball_on_field or take_pic: 
                image = np.array(race.render_data[0].image)
                Image.fromarray(image).save(args.save_dir / ('imageC_%06d.png' % count))
                with open(path.join(args.save_dir,('imageC_%06d.csv' % count)), 'w') as f:
                    if ball_on_field: f.write("1")
                    else:             f.write("0")
                count += 1
                if count >= args.num_images:
                    print('Done')
                    break
                elif not ball_on_field and count >= args.num_images//2:
                    ball_on_field = True
                    state.set_ball_location([0,0.5,0])


        # Make sure we play in real time
        n += 1
        delta_d = n * config.step_size - (time() - t0)
        if delta_d > 0:
            ui.sleep(delta_d)

    race.stop()
    del race
    pystk.clean()
