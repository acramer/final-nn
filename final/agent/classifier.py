import torch
import torch.nn as nn
import torch.nn.functional as F


class CNNClassifier(torch.nn.Module):
    class Block(torch.nn.Module):
        def __init__(self, n_input, n_output, kernel_size=3, stride=2):
            super().__init__()
            self.c1 = torch.nn.Conv2d(n_input, n_output, kernel_size=kernel_size, padding=kernel_size // 2, stride=stride)
            self.c2 = torch.nn.Conv2d(n_output, n_output, kernel_size=kernel_size, padding=kernel_size // 2)
            self.c3 = torch.nn.Conv2d(n_output, n_output, kernel_size=kernel_size, padding=kernel_size // 2)
            self.b1 = torch.nn.BatchNorm2d(n_output)
            self.b2 = torch.nn.BatchNorm2d(n_output)
            self.b3 = torch.nn.BatchNorm2d(n_output)
            self.skip = torch.nn.Conv2d(n_input, n_output, kernel_size=1, stride=stride)

        def forward(self, x):
            return F.relu(self.b3(self.c3(F.relu(self.b2(self.c2(F.relu(self.b1(self.c1(x)))))))) + self.skip(x))

    class UpBlock(torch.nn.Module):
        def __init__(self, n_input, n_output, kernel_size=3, stride=2):
            super().__init__()
            self.c1 = torch.nn.ConvTranspose2d(n_input, n_output, kernel_size=kernel_size, padding=kernel_size // 2, stride=stride, output_padding=1)

        def forward(self, x):
            return F.relu(self.c1(x))

    
    def __init__(self, layers=[16, 32, 64, 128], n_output_channels=2, kernel_size=3, use_skip=True):
        super().__init__()
        self.params = [layers[:], n_output_channels, kernel_size, use_skip]
        # self.input_mean = torch.Tensor([0.3521554, 0.30068502, 0.28527516])
        # self.input_std = torch.Tensor([0.18182722, 0.18656468, 0.15938024])

        self.input_norm = torch.nn.BatchNorm2d(3)
        
        c = 3
        self.use_skip = use_skip
        self.n_conv = len(layers)
        skip_layer_size = [3] + layers[:-1]

        for i, l in enumerate(layers):
            self.add_module('conv%d' % i, self.Block(c, l, kernel_size, 2))
            c = l
        for i, l in list(enumerate(layers))[::-1]:
            self.add_module('upconv%d' % i, self.UpBlock(c, l, kernel_size, 2))
            c = l
            if self.use_skip:
                c += skip_layer_size[i]

        self.classifier = torch.nn.Conv2d(c, n_output_channels, 1)
        self.pool = torch.nn.AvgPool2d(5, stride=4)
        
    def forward(self, x):
        z = self.input_norm(x)
        z = self.pool(z)
        up_activation = []
        for i in range(self.n_conv):
            # Add all the information required for skip connections
            up_activation.append(z)
            z = self._modules['conv%d'%i](z)

        for i in reversed(range(self.n_conv)):
            z = self._modules['upconv%d'%i](z)
            # Fix the padding
            z = z[:, :, :up_activation[i].size(2), :up_activation[i].size(3)]
            # Add the skip connection
            if self.use_skip:
                z = torch.cat([z, up_activation[i]], dim=1)
        z = self.classifier(z)
        return z.mean(dim=[2,3])

    def classify(self, x):
        y = self.forward(x)
        return torch.argmax(y, dim=1)
    
def save_model(model, des=''):
    from torch import save
    from os import path
    if isinstance(model, CNNClassifier):
        return save((model.state_dict(), model.params), path.join(path.dirname(path.abspath(__file__)), 'cnn'+des+'.th'))
    raise ValueError("model type '%s' not supported!" % str(type(model)))


def load_model(file = None):
    from os import path
    if file is None:
        f = torch.load(path.join(path.dirname(path.abspath(__file__)), 'cnn.th'), map_location='cpu')
    else:
        f = torch.load(path.abspath(file), map_location='cpu')
    std, params = f, []
    if isinstance(f, tuple):
        std, params = f
    r = CNNClassifier(*params)
    r.load_state_dict(std)
    return r


class FCN(torch.nn.Module):
    # Convolutional Block
    class Block(nn.Module):
        def __init__(self, in_channels, out_channels, kernel_size=3, stride=1):
            super().__init__()
            self.c = nn.Sequential(
                nn.Conv2d( in_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=stride),
                nn.Conv2d( out_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1),
                nn.Conv2d( out_channels, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1),
            )
            self.relu = nn.ReLU()
        
        def forward(self, x):
            y = self.c(x)
            return y

    # Residual Block
    class R_Block(nn.Module):
        def __init__(self, in_channels, out_channels, kernel_size=3, stride=1):
            super().__init__()
            self.c0 = nn.Conv2d( in_channels, in_channels//2, kernel_size=kernel_size, padding=kernel_size//2, stride=stride, bias=False)
            self.c2 = nn.Conv2d( in_channels//2, out_channels//2, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            self.c3 = nn.Conv2d( out_channels//2, out_channels, kernel_size=kernel_size, padding=kernel_size//2, stride=1, bias=False)
            self.b0 = nn.BatchNorm2d(in_channels//2)
            self.b2 = nn.BatchNorm2d(out_channels//2)
            self.b3 = nn.BatchNorm2d(out_channels)
            self.relu = nn.ReLU()

            self.i3 = nn.Identity()
            if in_channels != out_channels or stride != 1:
                self.i3 = nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=stride)
        
        def forward(self, x):
            g3 = self.i3(x)
            y = self.relu(self.b0(self.c0(x)))
            y = self.relu(self.b2(self.c2(y)))
            y = self.relu(self.b3(self.c3(y) + g3))
            return y

    # Dense Block
    class D_Block(nn.Module):
        def __init__(self, channels=[8, 16, 32, 48, 32, 16, 8]):
            super().__init__()
            channels=[8, 16, 32, 48, 32, 16, 8]
            c=channels.copy()
            input_channels = 3
            output_channels = 5
            input_kernel_size = 3
            self.bi = nn.BatchNorm2d(input_channels)
            self.bc0 = nn.BatchNorm2d(c[0])
            self.bc1 = nn.BatchNorm2d(c[1])
            self.bc2 = nn.BatchNorm2d(c[2])
            self.bc3 = nn.BatchNorm2d(c[3])
            self.bc4 = nn.BatchNorm2d(c[4])
            self.bc5 = nn.BatchNorm2d(c[5])
            self.bc6 = nn.BatchNorm2d(c[6])
            self.c0 = nn.Conv2d(     input_channels, c[0],            kernel_size=input_kernel_size, stride=2, padding=input_kernel_size//2, bias=False)
            self.c1 = nn.Conv2d(         sum(c[:1]), c[1],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c2 = nn.Conv2d(         sum(c[:2]), c[2],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c3 = nn.Conv2d(         sum(c[:3]), c[3],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c4 = nn.ConvTranspose2d(sum(c[:4]), c[4],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c5 = nn.ConvTranspose2d(sum(c[:5]), c[5],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c6 = nn.ConvTranspose2d(sum(c[:6]), c[6],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c7 = nn.ConvTranspose2d(sum(c[:7]), output_channels, kernel_size=input_kernel_size, stride=2, output_padding=1, padding=input_kernel_size//2, bias=False)
            self.relu = nn.ReLU()

        def forward(self, x):
            x  = self.bi(x)
            y  = self.relu(self.bc0(self.c0(x))) # 3 ->  8
            y1 = self.relu(self.bc1(self.c1(y))) # 8 -> 16
            y=torch.cat((y, y1),dim=1)
            y2 = self.relu(self.bc2(self.c2(y))) #16 -> 32
            y=torch.cat((y, y2), dim=1)
            y3 = self.relu(self.bc3(self.c3(y))) #32 -> 48
            y=torch.cat((y, y3), dim=1)
            y4 = self.relu(self.bc4(self.c4(y))) #48 -> 32 
            y=torch.cat((y, y4), dim=1)
            y5 = self.relu(self.bc5(self.c5(y)))  #32 -> 16 #64 -> 16
            y=torch.cat((y, y5), dim=1)
            y6 = self.relu(self.bc6(self.c6(y)))  #16 ->  8 #32 ->  8
            y=torch.cat((y, y6), dim=1)
            y = self.relu(self.c7(y))  # 8 ->  6 #16 ->  6
            if x.shape[3] == 1: y = y[:,:,:,:1]
            if x.shape[2] == 1: y = y[:,:,:1,:]
            return y
            
    # Experimental Block
    class E_Block(nn.Module):
        def __init__(self, channels=[8, 40, 80, 200, 80, 40, 8]):
            super().__init__()
            channels=[8, 16, 32, 48, 32, 16, 8]
            c=channels.copy()
            input_channels = 3
            output_channels = 5
            input_kernel_size = 3
            self.bi = nn.BatchNorm2d(input_channels)
            self.bc0 = nn.BatchNorm2d(c[0])
            self.bc1 = nn.BatchNorm2d(c[1])
            self.bc2 = nn.BatchNorm2d(c[2])
            self.bc3 = nn.BatchNorm2d(c[3])
            self.bc4 = nn.BatchNorm2d(c[4])
            self.bc5 = nn.BatchNorm2d(c[5])
            self.bc6 = nn.BatchNorm2d(c[6])
            self.c0 = nn.Conv2d(     input_channels, c[0],            kernel_size=input_kernel_size, stride=2, padding=input_kernel_size//2, bias=False)
            self.c1 = nn.Conv2d(               c[0], c[1],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c2 = nn.Conv2d(               c[1], c[2],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c3 = nn.Conv2d(               c[2], c[3],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c4 = nn.ConvTranspose2d(sum(c[:4]), c[4],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c5 = nn.ConvTranspose2d(sum(c[:5]), c[5],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c6 = nn.ConvTranspose2d(sum(c[:6]), c[6],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c7 = nn.ConvTranspose2d(sum(c[:7]), output_channels, kernel_size=input_kernel_size, stride=2, output_padding=1, padding=input_kernel_size//2, bias=False)
            self.relu = nn.ReLU()

        def forward(self, x):
            x  = self.bi(x)
            y  = self.relu(self.bc0(self.c0(x))) # 3 ->  8
            y1 = self.relu(self.bc1(self.c1(y))) # 8 -> 16
            y2 = self.relu(self.bc2(self.c2(y1))) #16 -> 32
            y3 = self.relu(self.bc3(self.c3(y2))) #32 -> 48
            y=torch.cat((y, y1, y2, y3), dim=1)
            y4 = self.relu(self.bc4(self.c4(y))) #48 -> 32 
            y=torch.cat((y, y4), dim=1)
            y5 = self.relu(self.bc5(self.c5(y)))  #32 -> 16 #64 -> 16
            y=torch.cat((y, y5), dim=1)
            y6 = self.relu(self.bc6(self.c6(y)))  #16 ->  8 #32 ->  8
            y=torch.cat((y, y6), dim=1)
            y = self.relu(self.c7(y))  # 8 ->  6 #16 ->  6
            if x.shape[3] == 1: y = y[:,:,:,:1]
            if x.shape[2] == 1: y = y[:,:,:1,:]
            return y

    # Experimental Block #2
    class E2_Block(nn.Module):
        def __init__(self, channels=[8, 16, 32, 48, 32, 16, 8]):
            super().__init__()
            #channels=[8, 16, 24, 32, 40, 48, 40, 32, 24, 16, 8]
            channels=[8, 16, 24, 40, 56, 40, 24, 16, 8]
            c=channels.copy()
            input_channels = 3
            output_channels = 5
            input_kernel_size = 3
            self.bi = nn.BatchNorm2d(input_channels)
            self.bc0 = nn.BatchNorm2d(c[0])
            self.bc1 = nn.BatchNorm2d(c[1])
            self.bc2 = nn.BatchNorm2d(c[2])
            self.bc3 = nn.BatchNorm2d(c[3])
            self.bc4 = nn.BatchNorm2d(c[4])
            self.bc5 = nn.BatchNorm2d(c[5])
            self.bc6 = nn.BatchNorm2d(c[6])
            self.bc7 = nn.BatchNorm2d(c[7])
            self.bc8 = nn.BatchNorm2d(c[8])
            self.c0 = nn.Conv2d(     input_channels, c[0],            kernel_size=input_kernel_size, stride=2, padding=input_kernel_size//2, bias=False)
            self.c1 = nn.Conv2d(               c[0], c[1],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c2 = nn.Conv2d(               c[1], c[2],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c3 = nn.Conv2d(               c[2], c[3],            kernel_size=input_kernel_size, stride=1, padding=input_kernel_size//2, bias=False)
            self.c4 = nn.ConvTranspose2d(sum(c[:4]), c[4],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c5 = nn.ConvTranspose2d(sum(c[:5]), c[5],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c6 = nn.ConvTranspose2d(sum(c[:6]), c[6],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c7 = nn.ConvTranspose2d(sum(c[:7]), c[7],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c8 = nn.ConvTranspose2d(sum(c[:8]), c[8],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c11 = nn.ConvTranspose2d(sum(c[:9]), output_channels, kernel_size=input_kernel_size, stride=2, output_padding=1, padding=input_kernel_size//2, bias=False)
            self.relu = nn.ReLU()

        def forward(self, x):
            x  = self.bi(x)
            y  = self.relu(self.bc0(self.c0(x)))
            y1 = self.relu(self.bc1(self.c1(y)))
            y2 = self.relu(self.bc2(self.c2(y1)))
            y3 = self.relu(self.bc3(self.c3(y2)))
            y=torch.cat((y, y1, y2, y3), dim=1)
            y4 = self.relu(self.bc4(self.c4(y)))
            y=torch.cat((y, y4), dim=1)
            y5 = self.relu(self.bc5(self.c5(y)))
            y=torch.cat((y, y5), dim=1)
            y6 = self.relu(self.bc6(self.c6(y)))
            y=torch.cat((y, y6), dim=1)
            y7 = self.relu(self.bc7(self.c7(y)))
            y=torch.cat((y, y7), dim=1)
            y8 = self.relu(self.bc8(self.c8(y)))
            y=torch.cat((y, y8), dim=1)
            y = self.relu(self.c11(y))
            if x.shape[3] == 1: y = y[:,:,:,:1]
            if x.shape[2] == 1: y = y[:,:,:1,:]
            return y

    # Experimental Block #3
    class E3_Block(nn.Module):
        def __init__(self, channels=[8, 16, 32, 48, 64, 48, 32, 16, 8]):
            super().__init__()
            c=channels.copy()
            input_channels = 3
            output_channels = 5
            input_kernel_size = 3
            self.bi = nn.BatchNorm2d(input_channels)
            self.bc0 = nn.BatchNorm2d(c[0])
            self.bc1 = nn.BatchNorm2d(c[1])
            self.bc2 = nn.BatchNorm2d(c[2])
            self.bc3 = nn.BatchNorm2d(c[3])
            self.bc4 = nn.BatchNorm2d(c[4])
            self.bc5 = nn.BatchNorm2d(c[5])
            self.bc6 = nn.BatchNorm2d(c[6])
            self.bc7 = nn.BatchNorm2d(c[7])
            self.bc8 = nn.BatchNorm2d(c[8])
            self.c0 = nn.Conv2d(     input_channels, c[0],            kernel_size=input_kernel_size, stride=2, padding=input_kernel_size//2, bias=False)
            self.c1 = FCN.Block(c[0], c[1])
            self.c2 = FCN.Block(c[1], c[2])
            self.c3 = FCN.Block(c[2], c[3])
            self.c4 = FCN.R_Block(       sum(c[:4]), c[4])
            self.c5 = FCN.R_Block(       sum(c[:5]), c[5])
            self.c6 = FCN.R_Block(       sum(c[:6]), c[6])
            self.c7 = nn.ConvTranspose2d(sum(c[:7]), c[7],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c8 = nn.ConvTranspose2d(sum(c[:8]), c[8],            kernel_size=input_kernel_size, stride=1, output_padding=0, padding=input_kernel_size//2, bias=False)
            self.c9 = nn.ConvTranspose2d(sum(c[:9]), output_channels, kernel_size=input_kernel_size, stride=2, output_padding=1, padding=input_kernel_size//2, bias=False)
            self.relu = nn.ReLU()

        def forward(self, x):
            x  = self.bi(x)
            y  = self.relu(self.bc0(self.c0(x)))
            y1 = self.relu(self.bc1(self.c1(y)))
            y2 = self.relu(self.bc2(self.c2(y1)))
            y3 = self.relu(self.bc3(self.c3(y2)))
            y=torch.cat((y, y1, y2, y3), dim=1)
            y4 = self.relu(self.bc4(self.c4(y)))
            y=torch.cat((y, y4), dim=1)
            y5 = self.relu(self.bc5(self.c5(y)))
            y=torch.cat((y, y5), dim=1)
            y6 = self.relu(self.bc6(self.c6(y)))
            y=torch.cat((y, y6), dim=1)
            y7 = self.relu(self.bc7(self.c7(y)))
            y=torch.cat((y, y7), dim=1)
            y8 = self.relu(self.bc8(self.c8(y)))
            y=torch.cat((y, y8), dim=1)
            y = self.relu(self.c9(y))
            if x.shape[3] == 1: y = y[:,:,:,:1]
            if x.shape[2] == 1: y = y[:,:,:1,:]
            return y

    def __init__(self, channels=[9, 18, 36, 72, 36, 18, 9]):
        super().__init__()
        """
        Your code here.
        Hint: The FCN can be a bit smaller the the CNNClassifier since you need to run it at a higher resolution
        Hint: Use up-convolutions
        Hint: Use skip connections
        Hint: Use residual connections
        Hint: Always pad by kernel_size / 2, use an odd kernel_size
        """
        self.net = self.E3_Block()

    def forward(self, x):
        """
        Your code here
        @x: torch.Tensor((B,3,H,W))
        @return: torch.Tensor((B,6,H,W))
        Hint: Apply input normalization inside the network, to make sure it is applied in the grader
        Hint: Input and output resolutions need to match, use output_padding in up-convolutions, crop the output
              if required (use z = z[:, :, :H, :W], where H and W are the height and width of a corresponding strided
              convolution
        """
        return self.net(x)

if __name__ == '__main__':
    import torchvision
    from torchvision import transforms
    from PIL import Image
    from argparse import ArgumentParser
    parser = ArgumentParser("Collects a dataset for the high-level planner")
    parser.add_argument('images', nargs='+', help="Add any number of players. List python module names or `AI` for AI players). Teams alternate.")
    args = parser.parse_args()


    to_tensor = transforms.ToTensor()

    model = load_model('../puck_classifier.th')
    model.eval()
    if args.images is None: args.images = ['test.png']
    for f in args.images:
        im = to_tensor(Image.open(f))
        print(model(im[None, :, :, :]))

