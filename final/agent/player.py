import numpy as np
import torch

import torchvision
from torchvision import transforms

from .planner import Planner
from .planner import load_model as load_planner
from .classifier import CNNClassifier
from .classifier import load_model as load_classifier

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

class HockeyPlayer:
    """
       Your ice hockey player. You may do whatever you want here. There are three rules:
        1. no calls to the pystk lib rary (your code will not run on the tournament system if you do)
        2. There needs to be a deep network somewhere in the loop
        3. You code must run in 100 ms / frame on a standard desktop CPU (no for testing GPU)
        
        Try to minimize library dependencies, nothing that does not install through pip on linux.
    """
    
    """
       You may request to play with a different kart.
       Call `python3 -c "import pystk; pystk.init(pystk.GraphicsConfig.ld()); print(pystk.list_karts())"` to see all values.
    """
    def __init__(self, player_id = 0, debug=False, ctr_debug=False):
        """ 
        Set up a soccer player.
        The player_id starts at 0 and increases by one for each player added. You can use the player id to figure out your team (player_id % 2), or assign different roles to different agents.
        """
        self.puck_finder = load_planner('puck_finder.th').to(device)
        self.puck_on_screen = load_classifier('puck_classifier.th').to(device)
        self.puck_finder.eval()
        self.puck_on_screen.eval()

        self.kart = 'gavroche'

        self.previous_angle = 0
        self.reversing = False
        self.counter = 0
        if player_id % 2:
            self.atk = np.array([0, -70])
            self.defd = np.array([0, 70])
        else:
            self.atk = np.array([0, 70])
            self.defd = np.array([0, -70])

        self.last_pos_ball = np.array([0.0, 0.0])
        self.last_vels = []
        self.to_tensor = transforms.ToTensor()

        self.seen_puck = False
        self.wait_count = 0
        
    def act(self, image, player_info, ios=None, pos=None):
        W, H = image.shape[1], image.shape[0] 
        action = {'acceleration': 0, 'brake': False, 'drift': False, 'nitro': False, 'rescue': False, 'steer': 0}
        acc = -1
        brake = False
        steer = 0
        drift = False

        img = self.to_tensor(image)[None,:,:,:].to(device)

        pos_me =   to_numpy(player_info.kart.location)
        front_me = to_numpy(player_info.kart.front)

        pred_ios_ball = bool(np.argmax(self.puck_on_screen(img).detach().numpy()))
        ios_ball = pred_ios_ball
        im_coords = self.puck_finder(img).detach().numpy()[0]
        pos_ball = to_world(im_coords, player_info.camera, W, H, height=0.35)

        if pos_ball[1] < 2:
            pos_ball = self.last_pos_ball
        else:
            pos_ball = to_numpy(pos_ball)
            self.last_pos_ball = pos_ball

        if self.wait_count < 20 or player_info.kart.location[1] > 0.5 or (not ios_ball and not self.seen_puck):
            self.wait_count += 1
            return {'acceleration': 0, 'brake': False, 'drift': False, 'nitro': False, 'rescue': False, 'steer': 0}, im_coords, pred_ios_ball
        else: self.seen_puck = True

        if len(self.last_vels) >= 5:
            if all(np.array(self.last_vels) < 0.5):
                self.last_vels = []
                action['rescue'] = True
                return {'acceleration': 0, 'brake': False, 'drift': False, 'nitro': False, 'rescue': True, 'steer': 0}, im_coords, pred_ios_ball
            self.last_vels.pop(0)
        vel = np.linalg.norm(player_info.kart.velocity)
        self.last_vels.append(vel)

        dist_to_goal = np.linalg.norm(pos_me - self.atk)
        dist_to_puck = np.linalg.norm(pos_me - pos_ball)

        ori_me = get_vector_from_this_to_that(pos_me, front_me)
        ori_to_goal = get_vector_from_this_to_that(pos_me, self.atk)
        ori_to_puck = get_vector_from_this_to_that(pos_me, pos_ball-ori_to_goal*1.1)

        angle_me_puck = calculate_angle(ori_me, ori_to_puck)

        
        # if not reversing, but if its far away fuck reversing
        if ios_ball and (not self.reversing):
            # if we entered and are reversing it means
            # the puck is far away
            if self.reversing:
                self.reversing = False

            # first mode of moving forward
            self.previous_angle = angle_me_puck
            action['steer'] = (angle_me_puck)/(np.pi/2)
            action['acceleration'] = 1

        elif ios_ball or abs(angle_me_puck) > 80:
            # if this condition of puck on your side has been achieved and arent reversing, then do so
            if not self.reversing:
                self.reversing = True

            #if you are reversing, only stop if angle is within 10 degrees
            else:
                if dist_to_puck > 15 or abs(angle_me_puck) < 15:
                    self.reversing = False
                    
            action['brake'] = True
            action['acceleration'] = 0
            action['steer'] = -1*self.previous_angle

        else:
            if not self.reversing:
                # not yet reversing set to True and counter
                self.reversing = True

            action['brake'] = True
            action['acceleration'] = 0
            action['steer'] = -1*self.previous_angle
        
        return action, im_coords, pred_ios_ball



def to_world(aim_point_image, camera, screen_width, screen_height, height=0):
    proj, view = np.array(camera.projection).T, np.array(camera.view).T
    x, y, W, H = *aim_point_image, screen_width, screen_height
    pv_inv = np.linalg.pinv(proj @ view)
    xy, d = pv_inv.dot([float(x) / (W / 2) - 1, 1 - float(y) / (H / 2), 0, 1]), pv_inv[:, 2]
    x0, x1 = xy[:-1] / xy[-1], (xy+d)[:-1] / (xy+d)[-1]
    t = (height-x0[1]) / (x1[1] - x0[1])
    if t < 1e-3 or t > 10:
        # Project the point forward by a certain distance, if it would end up behind
        t = 10
    return t * x1 + (1-t) * x0

def get_vector_from_this_to_that(me, obj, normalize=True):
    """
    Expects numpy arrays as input
    """
    vector = obj - me

    if normalize:
        return vector / np.linalg.norm(vector)

    return vector

def calculate_angle(v1, v2):
    return (1 if (v1[1]*v2[0]-v1[0]*v2[1]) > 0 else -1)*np.degrees(np.arccos(np.dot(v1, v2)/(np.linalg.norm(v1)*np.linalg.norm(v2))))

def to_numpy(location):
    """
    Don't care about location[1], which is the height
    """
    return np.float32([location[0], location[2]])
