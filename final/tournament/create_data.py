import matplotlib.pyplot as plt
import matplotlib.lines as lines
from matplotlib.lines import Line2D

from pathlib import Path
from PIL import Image
import argparse
import pystk
from time import time
import numpy as np

import random
from scipy.spatial.transform import Rotation as R
from math import cos, sin, pi

import importlib

def action_dict(action):
    return {k: getattr(action, k) for k in ['acceleration', 'brake', 'steer', 'fire', 'drift']}

def to_numpy(location):
    """
    Don't care about location[1], which is the height
    """
    return np.float32([location[0], location[2]])


def get_vector_from_this_to_that(me, obj, normalize=True):
    """
    Expects numpy arrays as input
    """
    vector = obj - me

    if normalize:
        return vector / np.linalg.norm(vector)

    return vector

def to_image(config, x, camera):
    proj, view = np.array(camera.projection).T, np.array(camera.view).T
    W, H = config.screen_width, config.screen_height
    p = proj @ view @ np.array(list(x) + [1])
    return np.array([W / 2 * (p[0] / p[-1] + 1), H / 2 * (1 - p[1] / p[-1])])

def on_screen(config, x, camera, angle=0):
    if abs(angle) > 100: return False
    coords = to_image(config, x, camera)
    return coords[0] > 0 and coords[0] < 128 and coords[1] > 0 and coords[1] < 96

def to_world(config, aim_point_image, camera, height=0):
    proj, view = np.array(camera.projection).T, np.array(camera.view).T
    x, y, W, H = *aim_point_image, config.screen_width, config.screen_height
    pv_inv = np.linalg.pinv(proj @ view)
    xy, d = pv_inv.dot([float(x) / (W / 2) - 1, 1 - float(y) / (H / 2), 0, 1]), pv_inv[:, 2]
    x0, x1 = xy[:-1] / xy[-1], (xy+d)[:-1] / (xy+d)[-1]
    t = (height-x0[1]) / (x1[1] - x0[1])
    if t < 1e-3 or t > 10:
        # Project the point forward by a certain distance, if it would end up behind
        t = 10
    return t * x1 + (1-t) * x0

def collect_puck_data(save_dir, frames=1000, pics_per_frame=25, p_num=0):
    from os import path
    if save_dir:
        save_dir.mkdir(parents=True, exist_ok=True)

    g_config = pystk.GraphicsConfig.hd()
    g_config.screen_width = 400
    g_config.screen_height = 300
    pystk.init(g_config)

    config = pystk.RaceConfig()
    config.num_kart = 1
    config.players[0].kart = 'gavroche'

    config.players[0].controller = pystk.PlayerConfig.Controller.PLAYER_CONTROL
    config.players[0].team = args.team

    config.track = "icy_soccer_field"
    config.mode = config.RaceMode.SOCCER

    race = pystk.Race(config)
    race.start()
    race.step()

    state = pystk.WorldState()

    count = 0
    state.update()
    for _ in range(20): race.step()
    for t in range(frames):
        # Vector and Orientation Calculations
        if count % pics_per_frame == 0:
            # Part 1 - Randomly place kart in Boundary A
            psi = random.random()*360
            rko = R.from_euler('y',psi,degrees=True).as_quat()
            rkx, rky = (random.random()-0.5)*50,(random.random()-0.5)*80
            state.set_kart_location(state.players[0].kart.id, [rkx, 1, rky], rko)
            # Select Random distance and random angle
            dist = random.random()*(abs(random.choice([rkx-35,rkx+35,rky-50,rky+50]))-1.5)+1.5
            angle = (random.random()*90-45+psi) * pi/180 - pi/2
            # Calculate Vector to puck
            pos_ball = (rkx + cos(angle)*dist, rky - sin(angle)*dist)
            # Ensure it is within boundaries
            pos_ball = (max(min(pos_ball[0], 35), -35), max(min(pos_ball[1], 50), -50))
            # Change Puck Position
            state.set_ball_location([pos_ball[0],0.5,pos_ball[1]])
            state.update()
            race.step()
            race.step()
            race.step()
            race.step()
        # elif count % 2 == 0:
        # Select Random distance and random angle
        dist = random.random()*(abs(random.choice([rkx-35,rkx+35,rky-50,rky+50]))-1.5)+1.5
        angle = (random.random()*90-45+psi) * pi/180 - pi/2
        # Calculate Vector to puck
        pos_ball = (rkx + cos(angle)*dist, rky - sin(angle)*dist)
        # Ensure it is within boundaries
        pos_ball = (max(min(pos_ball[0], 35), -35), max(min(pos_ball[1], 50), -50))
        # Change Puck Position
        state.set_ball_location([pos_ball[0],0.5,pos_ball[1]])
        state.update()
        #if save_dir is not None:
        race.step()
        image = np.array(race.render_data[0].image)
        Image.fromarray(image).save(args.save_dir / ('image_%06d.png' % (p_num)))
        im_ball = to_image(g_config, state.soccer.ball.location, state.players[0].camera)
        with open(path.join(save_dir,'image_%06d.csv' % (p_num)), 'w') as f:
            f.write("{:.1f},{:.1f}".format(im_ball[0], im_ball[1]))
        p_num += 1
        count += 1


    race.stop()
    del race
    pystk.clean()
    return p_num

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--frames', type=int, default=1000)
    parser.add_argument('-n', '--pics_per', type=int, default=25)
    parser.add_argument('-p', '--p_num', type=int, default=0)
    parser.add_argument('-k', '--kart', default='')
    parser.add_argument('--team', type=int, default=0, choices=[0, 1])
    parser.add_argument('--save_dir', type=Path, required=False)
    args = parser.parse_args()

    if args.save_dir:
        args.save_dir.mkdir(parents=True, exist_ok=True)

    collect_puck_data(args.save_dir, frames=args.frames, pics_per_frame=args.pics_per, p_num=args.p_num)

