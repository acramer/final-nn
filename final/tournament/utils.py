import torch
import torch.nn.functional as F
import pystk
import numpy as np
from PIL import Image

from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import torchvision.transforms.functional as TF
from . import dense_transforms

import random

from argparse import ArgumentParser
import importlib

RESCUE_TIMEOUT = 30
TRACK_OFFSET = 15
DATASET_PATH = 'drive_data'

class Player:
    def __init__(self, player, team=0):
        self.player = player
        self.team = team

    @property
    def config(self):
        return pystk.PlayerConfig(controller=pystk.PlayerConfig.Controller.PLAYER_CONTROL, kart=self.player.kart, team=self.team)
    
    #def __call__(self, image, player_info):
    def __call__(self, image, player_info, ios):
        return self.player.act(image, player_info, ios)

class AI_Player:
    def __init__(self, team=0):
        self.team = team

    @property
    def config(self):
        return pystk.PlayerConfig(controller=pystk.PlayerConfig.Controller.AI_CONTROL, team=self.team)

class Tournament:
    _singleton = None

    def __init__(self, players, screen_width=400, screen_height=300, track='icy_soccer_field'):
        assert Tournament._singleton is None, "Cannot create more than one Tournament object"
        Tournament._singleton = self

        self.graphics_config = pystk.GraphicsConfig.hd()
        self.graphics_config.screen_width = screen_width
        self.graphics_config.screen_height = screen_height
        pystk.init(self.graphics_config)

        self.race_config = pystk.RaceConfig(num_kart=len(players), track=track, mode=pystk.RaceConfig.RaceMode.SOCCER)
        self.race_config.players.pop()
        
        self.active_players = []
        for p in players:
            if p is not None:
                self.race_config.players.append(p.config)
                self.active_players.append(p)
        
        self.k = pystk.Race(self.race_config)

        self.k.start()
        self.k.step()

    def play(self, save=None, max_frames=50):
        state = pystk.WorldState()
        if save is not None:
            import PIL.Image
            import PIL.ImageDraw
            import os
            if not os.path.exists(save):
                os.makedirs(save)

        for t in range(max_frames):
            print('\rframe %d' % t, end='\r')

            state.update()

            list_actions = []
            for i, p in enumerate(self.active_players):
                player = state.players[i]
                image = np.array(self.k.render_data[i].image)
                
                action = pystk.Action()


                pos_ball = to_numpy(state.soccer.ball.location)
                pos_me = to_numpy(state.players[i].kart.location)
                front_me = to_numpy(state.players[i].kart.front)

                ori_me = get_vector_from_this_to_that(pos_me, front_me)
                ori_to_ball = get_vector_from_this_to_that(pos_me, pos_ball)

                angle = (1 if (ori_me[1]*ori_to_ball[0]-ori_me[0]*ori_to_ball[1]) > 0 else -1)*np.degrees(np.arccos(np.dot(ori_me, ori_to_ball)))
                is_on_screen = self._on_screen(state.soccer.ball.location, state.players[i].camera, angle)

                player_action, pred_ap, pred_ios = p(image, player, is_on_screen)


                #player_action, pred_ap, pred_ios = p(image, player)
                for a in player_action:
                    setattr(action, a, player_action[a])
                
                list_actions.append(action)

                if save is not None:
                    image_p = image[:,:,:]
                    point = np.array([np.ones((3,3))*255,np.zeros((3,3)),np.zeros((3,3))]).transpose()
                    #else:        point = np.array([np.zeros((3,3)),np.ones((3,3))*255,np.zeros((3,3))]).transpose()
                    pred_x, pred_y = min(image_p.shape[0]-3,max(0,int(pred_ap[0])-1)), min(image_p.shape[1]-3,max(0,int(pred_ap[1])-1))
                    image_p[pred_x:pred_x+3, pred_y:pred_y+3, :] = point 
                    image_p = PIL.Image.fromarray(image_p)
                    d = PIL.ImageDraw.Draw(image_p)
                    d.text((0,0), str(pred_ap), fill=(0,255,0) if pred_ios else (255,0,0))
                    #d.text((10,20), '+' if pred_ios else '-', fill=(0,255,0) if pred_ios else (255,0,0))
                    #d.text((pred_ap[0], pred_ap[1]), 'o', fill=(255,255,0))
                    image_p.save(os.path.join(save, 'player%02d_%05d.png' % (i, t)))

            s = self.k.step(list_actions)
            if not s:  # Game over
                break

        if save is not None:
            import subprocess
            for i, p in enumerate(self.active_players):
                dest = os.path.join(save, 'player%02d' % i)
                output = save + '_player%02d.mp4' % i
                subprocess.call(['/usr/local/bin/ffmpeg', '-y', '-framerate', '10', '-i', dest + '_%05d.png', output])
        if hasattr(state, 'soccer'):
            return state.soccer.score
        return state.soccer_score

    def _on_screen(self, x, camera, angle=0):
        W, H = self.graphics_config.screen_width, self.graphics_config.screen_height
        if abs(angle) > 100: return False
        coords = self._to_image(x, np.array(camera.projection).T, np.array(camera.view).T)
        return coords[0] > 0 and coords[0] < W and coords[1] > 0 and coords[1] < H

    def _to_image(self, x, proj, view):
        W, H = self.graphics_config.screen_width, self.graphics_config.screen_height
        p = proj @ view @ np.array(list(x) + [1])
        return np.array([W / 2 * (p[0] / p[-1] + 1), H / 2 * (1 - p[1] / p[-1])])

    def close(self):
        self.k.stop()
        del self.k


class SuperTuxDataset(Dataset):
    def __init__(self, dataset_path=DATASET_PATH, transform=dense_transforms.ToTensor()):
        from PIL import Image
        from glob import glob
        from os import path
        self.data = []
        for f in glob(path.join(dataset_path, '*.csv')):
            i = Image.open(f.replace('.csv', '.png'))
            i.load()
            self.data.append((i, np.loadtxt(f, dtype=np.float32, delimiter=',')))
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        data = self.data[idx]
        data = self.transform(*data)
        return data

def load_data(dataset_path=DATASET_PATH, transform=dense_transforms.ToTensor(), num_workers=0, batch_size=128):
    dataset = SuperTuxDataset(dataset_path, transform=transform)
    return DataLoader(dataset, num_workers=num_workers, batch_size=batch_size, shuffle=True, drop_last=True)

class SuperTuxHeatmapDataset(Dataset):
    def __init__(self, dataset_path, transform=dense_transforms.ToTensor(), min_size=20):
        from PIL import Image
        from glob import glob
        from os import path
        #self.data = []
        self.puck_data, self.images, self.enemy_data, self.ally_data, self.pickup_data = [], [], [], [], []
        #for f in glob(path.join(dataset_path, '*.csv')):
        for f in glob(path.join(dataset_path, '*.png')):
            #i = Image.open(f.replace('.csv', '.png'))
            i = Image.open(f)
            i.load()
            #self.data.append((i, np.loadtxt(f, dtype=np.float32, delimiter=',')))
            self.images.append(i)

            # TODO: Check existance for each of the following
            self.puck_data.append(np.loadtxt('puck_'+f.replace('.png', '.csv'), dtype=np.float32, delimiter=','))
            self.enemy_data.append(np.loadtxt('enemy_'+f.replace('.png', '.csv'), dtype=np.float32, delimiter=','))
            self.ally_data.append(np.loadtxt('ally_'+f.replace('.png', '.csv'), dtype=np.float32, delimiter=','))
            self.pickup_data.append(np.loadtxt('pickup_'+f.replace('.png', '.csv'), dtype=np.float32, delimiter=','))
        self.transform = transform

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        import numpy as np
        b = self.files[idx]
        im = Image.open(b + '_im.jpg')
        nfo = np.load(b + '_boxes.npz')
        data = im, self._filter(nfo['karts']), self._filter(nfo['bombs']), self._filter(nfo['pickup'])
        if self.transform is not None:
            data = self.transform(*data)
        return data

def load_heatmap_data(dataset_path, num_workers=0, batch_size=32, **kwargs):
    dataset = DetectionSuperTuxDataset(dataset_path, **kwargs)
    return DataLoader(dataset, num_workers=num_workers, batch_size=batch_size, shuffle=True, drop_last=True)

class SuperTuxClassDataset(Dataset):
    def __init__(self, dataset_path, transform=None):
        from PIL import Image
        from glob import glob
        from os import path
        self.data = []
        for f in glob(path.join(dataset_path, '*.csv')):
            i = Image.open(f.replace('.csv', '.png'))
            i.load()
            self.data.append((i, np.loadtxt(f, dtype=np.float32, delimiter=',')))
        self.transform = transform if transform is not None else transforms.ToTensor()

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return (self.transform(self.data[idx][0]), self.data[idx][1])

def load_classifier_data(dataset_path, num_workers=0, batch_size=128, **kwargs):
    dataset = SuperTuxClassDataset(dataset_path, **kwargs)
    return DataLoader(dataset, num_workers=num_workers, batch_size=batch_size, shuffle=True, drop_last=True)

def quat_rot(r, v, inverse=False):
    inv = 1 - 2 * float(inverse)
    return np.array([(1 - 2 * (r[(i + 1) % 3] ** 2 + r[(i + 2) % 3] ** 2)) * v[i] +
                     2 * (r[i] * r[(i + 1) % 3] - r[(i + 2) % 3] * r[3] * inv) * v[(i + 1) % 3] +
                     2 * (r[i] * r[(i + 2) % 3] + r[(i + 1) % 3] * r[3] * inv) * v[(i + 2) % 3] for i in range(3)])


class PyTux:
    _singleton = None

    def __init__(self, players, screen_width=400, screen_height=300, track='icy_soccer_field'):
        assert PyTux._singleton is None, "Cannot create more than one pytux object"
        PyTux._singleton = self
        self.graphics_config = pystk.GraphicsConfig.hd()
        self.graphics_config.screen_width = screen_width
        self.graphics_config.screen_height = screen_height
        pystk.init(self.graphics_config)

        self.race_config = pystk.RaceConfig(num_kart=len(players),
                                               track=track,
                                                mode=pystk.RaceConfig.RaceMode.SOCCER)
        #ai_ctr = self.race_config.players[0]
        ai_ctr = self.race_config.players.pop()
        
        self.active_players = []
        for i, p in enumerate(players):
            if p is not None:
                self.race_config.players.append(p.config)
                self.active_players.append(p)
            # else:
            #     tp = AI_Player(i % 2)
            #     self.race_config.players.append()
            #     self.active_players.append(p)

        ai_ctr.controller = pystk.PlayerConfig.Controller.AI_CONTROL
        ai_ctr.team = 1
        self.race_config.players.append(ai_ctr)
        
        self.k = None

    def _to_world(self, aim_point_image, proj, view, height=0):
        x, y, W, H = *aim_point_image, self.graphics_config.screen_width, self.graphics_config.screen_height
        pv_inv = np.linalg.pinv(proj @ view)
        xy, d = pv_inv.dot([float(x) / (W / 2) - 1, 1 - float(y) / (H / 2), 0, 1]), pv_inv[:, 2]
        x0, x1 = xy[:-1] / xy[-1], (xy+d)[:-1] / (xy+d)[-1]
        t = (height-x0[1]) / (x1[1] - x0[1])
        if t < 1e-3 or t > 10:
            # Project the point forward by a certain distance, if it would end up behind
            t = 10
        return t * x1 + (1-t) * x0

    @staticmethod
    def _point_on_track(distance, track, offset=0.0):
        """
        Get a point at `distance` down the `track`. Optionally applies an offset after the track segment if found.
        Returns a 3d coordinate
        """
        node_idx = np.searchsorted(track.path_distance[..., 1],
                                   distance % track.path_distance[-1, 1]) % len(track.path_nodes)
        d = track.path_distance[node_idx]
        x = track.path_nodes[node_idx]
        t = (distance + offset - d[0]) / (d[1] - d[0])
        return x[1] * t + x[0] * (1 - t)

    @staticmethod
    def _to_kart(x, kart):
        return quat_rot(kart.rotation, x - kart.location, True)

    def _to_image(self, x, proj, view):
        W, H = self.graphics_config.screen_width, self.graphics_config.screen_height
        p = proj @ view @ np.array(list(x) + [1])
        return np.array([W / 2 * (p[0] / p[-1] + 1), H / 2 * (1 - p[1] / p[-1])])

    def _on_screen(self, x, camera, angle=0):
        W, H = self.graphics_config.screen_width, self.graphics_config.screen_height
        if abs(angle) > 100: return False
        coords = self._to_image(x, np.array(camera.projection).T, np.array(camera.view).T)
        return coords[0] > 0 and coords[0] < W and coords[1] > 0 and coords[1] < H

    def rollout(self, max_frames=1000, verbose=False, data_callback=None):
        """
        Play a level (track) for a single round.
        :param track: Name of the track
        :param controller: low-level controller, see controller.py
        :param planner: high-level planner, see planner.py
        :param max_frames: Maximum number of frames to play for
        :param verbose: Should we use matplotlib to show the agent drive?
        :param data_callback: Rollout calls data_callback(time_step, image, 2d_aim_point)
                every step, used to store the data
        :return: Number of steps played
        """
        do_render = verbose or data_callback is not None
        if self.k is not None and do_render == self.k.config.render:
            self.k.restart()
            self.k.step()
        else:
            if self.k is not None:
                self.k.stop()
                del self.k

            # Random Kart
            for p in self.race_config.players:
                all_players = ['adiumy', 'amanda', 'beastie', 'emule', 'gavroche', 'gnu', 'hexley', 'kiki', 'konqi', 'nolok', 'pidgin', 'puffy', 'sara_the_racer', 'sara_the_wizard', 'suzanne', 'tux', 'wilber', 'xue']
                p.kart = all_players[np.random.choice(len(all_players))]
            # Random Difficulty
            self.race_config.difficulty = random.randint(0,2)
            # Random Team
            if random.randint(0,1):
                self.race_config.players[0].team = 0
                self.race_config.players[1].team = 1
            else:
                self.race_config.players[0].team = 1
                self.race_config.players[1].team = 0
            print('Team:{}|Kart:{}|Difficulty:{}'.format(self.race_config.players[0].team,self.race_config.players[0].kart,self.race_config.difficulty))
            self.k = pystk.Race(self.race_config)
            self.k.start()
            self.k.step()

        state = pystk.WorldState()

        for t in range(max_frames):
            print('\rframe %d' % t, end='\r')
            state.update()
            
            list_actions = []
            for i, p in enumerate(self.active_players):
                player = state.players[i]
                image = np.array(self.k.render_data[i].image)
                
                action = pystk.Action()
                player_action = p(image, player)
                for a in player_action:
                    setattr(action, a, player_action[a])
                
                list_actions.append(action)

            if data_callback is not None:
                # Vector and Orientation Calculations
                pos_ball = to_numpy(state.soccer.ball.location)
                pos_me = to_numpy(state.players[0].kart.location)
                front_me = to_numpy(state.players[0].kart.front)

                ori_me = get_vector_from_this_to_that(pos_me, front_me)
                ori_to_ball = get_vector_from_this_to_that(pos_me, pos_ball)

                angle = (1 if (ori_me[1]*ori_to_ball[0]-ori_me[0]*ori_to_ball[1]) > 0 else -1)*np.degrees(np.arccos(np.dot(ori_me, ori_to_ball)))
                is_on_screen = self._on_screen(state.soccer.ball.location, state.players[0].camera, angle)
                dist_to_puck = np.linalg.norm(pos_me-pos_ball)

                aim_point_world = state.soccer.ball.location
                # Fix by setting aim_point and setting camera
                ap = self._to_image(aim_point_world, np.array(state.players[0].camera.projection).T, np.array(state.players[0].camera.view).T)
                # if is_on_screen and abs(pos_me[1]) < 63 and abs(float(aim_point_world[1])) < 2 and dist_to_puck < 15:
                if not is_on_screen and abs(pos_me[1]) < 63:
                    data_callback(t, np.array(self.k.render_data[0].image), ap)

            s = self.k.step(list_actions)
            if not s:  # Game over
                break
        self.k.stop()
        self.k = None
        return t




    def close(self):
        """
        Call this function, once you're done with PyTux
        """
        if self.k is not None:
            self.k.stop()
            del self.k
        pystk.clean()

def to_numpy(location):
    """
    Don't care about location[1], which is the height
    """
    return np.float32([location[0], location[2]])


def get_vector_from_this_to_that(me, obj, normalize=True):
    """
    Expects numpy arrays as input
    """
    vector = obj - me

    if normalize:
        return vector / np.linalg.norm(vector)

    return vector

def spatial_argmax(logit):
    """
    Compute the soft-argmax of a heatmap
    :param logit: A tensor of size BS x H x W
    :return: A tensor of size BS x 2 the soft-argmax in normalized coordinates (-1 .. 1)
    """
    weights = F.softmax(logit.view(logit.size(0), -1), dim=-1).view_as(logit)
    return torch.stack(((weights.sum(1) * torch.linspace(-1, 1, logit.size(2)).to(logit.device)[None]).sum(1),
                        (weights.sum(2) * torch.linspace(-1, 1, logit.size(1)).to(logit.device)[None]).sum(1)), 1)


if __name__ == '__main__':
    # from .controller import control
    from argparse import ArgumentParser
    from os import makedirs


    # def noisy_control(aim_pt, vel):
    #     return control(aim_pt + np.random.randn(*aim_pt.shape) * aim_noise,
    #                    vel + np.random.randn() * vel_noise)


    parser = ArgumentParser("Collects a dataset for the high-level planner")
    # parser.add_argument('track', nargs='+')
    parser.add_argument('-o', '--output', default=DATASET_PATH)
    parser.add_argument('-n', '--n_images', default=10000, type=int)
    parser.add_argument('-m', '--steps_per_track', default=20000, type=int)
    parser.add_argument('--aim_noise', default=5, type=float)
    parser.add_argument('--vel_noise', default=5, type=float)
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('players', nargs='+', help="Add any number of players. List python module names or `AI` for AI players). Teams alternate.")
    args = parser.parse_args()


    players = []
    for i, player in enumerate(args.players):
        if player == 'AI':
            #players.append(None)
            players.append(AI_Player(i % 2))
        else:
            players.append(Player(importlib.import_module(player).HockeyPlayer(i), i % 2))

    try:
        makedirs(args.output)
    except OSError:
        pass
    pytux = PyTux(players)
    #for track in args.track:

    n = 0
    aim_noise, vel_noise = 0, 0


    def collect(_, im, pt):
        from PIL import Image
        from os import path
        global n
        id = n if n < args.n_images else np.random.randint(0, n + 1)
        if id < args.n_images:
            fn = path.join(args.output, 'hockey' + '_%05d' % id)
            Image.fromarray(im).save(fn + '.png')
            # with open(fn + '.csv', 'w') as f:
            #     f.write('%0.1f,%0.1f' % tuple(pt))
            with open(fn + '.csv', 'w') as f:
                f.write('0')
        else: n = args.steps_per_track
        n += 1


    while n < args.steps_per_track:
        step = pytux.rollout(max_frames=1000, verbose=args.verbose, data_callback=collect)
        #print(step)
        # Add noise after the first round
        # aim_noise, vel_noise = args.aim_noise, args.vel_noise

    pytux.close()

